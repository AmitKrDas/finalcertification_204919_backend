﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectManager.DataLayer;

//using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;

namespace ProjectManager.API.Tests
{

    [TestFixture]
    public class TestProjectController
    {
        [TestCase]
        public void GetAll_ShouldReturnAllProjects() {
            var context = new TestProjectManagerAppContext();
            //context.Projects.Add(GetDemoProduct());

            var controller = new ProjectsController();
            //var result = controller.GetProduct(3) as OkNegotiatedContentResult<Product>;
            var result = controller.GetProjects() as TestProjectDbSet;
            Assert.AreEqual(result.Count(), 21);
        }
    }
}
