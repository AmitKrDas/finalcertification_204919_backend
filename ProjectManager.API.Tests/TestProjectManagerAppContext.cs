﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectManager.DataLayer;

namespace ProjectManager.API.Tests
{
    class TestProjectManagerAppContext: IProjectManagerContext
    {
        public TestProjectManagerAppContext()
        {
            this.Projects = new TestProjectDbSet();
        }

        public DbSet<Projects> Projects { get; set; }

        

        public int SaveChanges()
        {
            return 0;
        }

        public void MarkAsModified(Projects item) { }
        public void Dispose() { }
    }
}
