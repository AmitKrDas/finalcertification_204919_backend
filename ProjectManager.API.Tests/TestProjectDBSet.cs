﻿using ProjectManager.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.API.Tests
{
    class TestProjectDbSet : TestDbSet<Projects>
    {
        public override Projects Find(params object[] keyValues)
        {
            return this.SingleOrDefault(projects => projects.Project_ID == (int)keyValues.Single());
        }
    }
}
