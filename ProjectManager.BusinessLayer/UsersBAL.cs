﻿using ProjectManager.DataLayer;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.BusinessLayer
{
    public class UsersBAL : IDisposable
    {

        private ProjectManagerEntities db = new ProjectManagerEntities();

        public UsersBAL() { }

        public UsersBAL(IProjectManagerContext context)
        {
            //db = context;
        }


        public IQueryable<Users> GetUsers()
        {
            return db.Users;
        }

        public Users GetUsers(int id)
        {
            return db.Users.Find(id);

        }


        public void UpdateUsers(Users users)
        {
            using (ProjectManagerEntities db = new ProjectManagerEntities())
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        var objUsers = db.Users.FirstOrDefault(x => x.User_ID == users.User_ID);
                        db.Entry(objUsers).CurrentValues.SetValues(users);
                        db.Entry(objUsers).State = EntityState.Modified;

                        db.SaveChanges();

                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }

                }
            }

        }

        public Users AddUsers(Users users)
        {
            using (ProjectManagerEntities db = new ProjectManagerEntities())
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.Users.Add(users);
                        db.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                    }
                }
            }

            return users;

        }

        public Users DeleteUsers(int id)
        {
            Users users = null;

            using (ProjectManagerEntities db = new ProjectManagerEntities())
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {

                    try
                    {
                        users = db.Users.Find(id);
                        if (users == null)
                        {
                            return users;
                        }

                        db.Users.Remove(users);
                        db.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                    }
                }
            }

            return users;
        }


        public bool UsersExists(int id)
        {
            return db.Users.Count(e => e.User_ID == id) > 0;
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}
