﻿using ProjectManager.DataLayer;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.BusinessLayer
{
   public class ParentTasksBAL : IDisposable
    {

        private ProjectManagerEntities db = new ProjectManagerEntities();

        public ParentTasksBAL() { }

        public ParentTasksBAL(IProjectManagerContext context)
        {
            //db = context;
        }


        public IQueryable<Parent_Tasks> GetParentTasks()
        {
            return db.Parent_Tasks;
        }

        public Parent_Tasks GetParentTasks(int id)
        {
            return db.Parent_Tasks.Find(id);
        }


        public void UpdateParentTasks(Parent_Tasks parent_Tasks)
        {
            using (ProjectManagerEntities db = new ProjectManagerEntities())
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        var objParentTask = db.Parent_Tasks.FirstOrDefault(x => x.Parent_ID == parent_Tasks.Parent_ID);

                        db.Entry(objParentTask).State = EntityState.Modified;

                        db.SaveChanges();

                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }

                }
            }

        }

        public Parent_Tasks AddParentTasks(Parent_Tasks parentTasks)
        {
            using (ProjectManagerEntities db = new ProjectManagerEntities())
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.Parent_Tasks.Add(parentTasks);
                        db.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                    }
                }
            }

            return parentTasks;
            
        }

        public Parent_Tasks DeleteParentTasks(int id)
        {
            Parent_Tasks parentTasks = null;

            using (ProjectManagerEntities db = new ProjectManagerEntities())
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {

                    try
                    {
                        parentTasks = db.Parent_Tasks.Find(id);
                        if (parentTasks == null)
                        {
                            return parentTasks;
                        }
                        var tasks = db.Tasks.Where(x => x.Parent_ID == id).ToList();
                        if (tasks.Count > 0)
                        {
                            tasks.ForEach(x => {
                                x.Parent_ID = null;
                                db.Entry(x).State = EntityState.Modified;
                            });
                        }
                        
                        db.Parent_Tasks.Remove(parentTasks);
                        db.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                    }
                }
            }

            return parentTasks;
        }


        public bool Parent_TasksExists(int id)
        {
            return db.Parent_Tasks.Count(e => e.Parent_ID == id) > 0;
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}
