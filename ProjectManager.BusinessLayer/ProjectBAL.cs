﻿using ProjectManager.DataLayer;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.BusinessLayer
{
    public class ProjectBAL: IDisposable
    {

        private ProjectManagerEntities db = new ProjectManagerEntities();

        public ProjectBAL() { }

        public ProjectBAL(IProjectManagerContext context)
        {
            //db = context;
        }


        public IEnumerable<Projects> GetProjects() {

            
            IQueryable<Projects> projects =  db.Projects;

            foreach (var item in projects)
            {
                if ((item.Tasks != null) && (item.Tasks.Count> 0))
                {
                    item.TasksCompleted = item.Tasks.Count(x => x.Status.Trim() == "completed");
                    item.NumberofTasks = item.Tasks.Count();
                }
            }

            return projects;
        }

        public Projects GetProjects(int id)
        {
            return db.Projects.Find(id);
            
        }

        public IQueryable<Projects> Search(string searchText)
        {
            return db.Projects.Where(t =>
            t.Project.Contains(searchText));
        }

        public void UpdateProjects(int id, Projects projects)
        {
            using (ProjectManagerEntities db = new ProjectManagerEntities())
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        var objProjects = db.Projects.FirstOrDefault(x => x.Project_ID == id);
                        db.Entry(objProjects).CurrentValues.SetValues(projects);
                        Users obj = projects.Users.FirstOrDefault(x => x.Project_ID == id);
                        Tasks objTasks = objProjects.Tasks.FirstOrDefault(x => x.Project_ID == id);

                        if (objProjects.Users != null) { objProjects.Users = null; }
                        if (objProjects.Tasks != null) { objProjects.Tasks = null; }
                        if (obj != null) { obj.Projects = null; obj.Tasks = null; }

                        db.Entry(objProjects).State = EntityState.Modified;

                        if (obj!=null)
                        {
                            db.Entry(obj).State = EntityState.Modified;
                        }
                        

                        db.SaveChanges();

                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }

                }
            }
            
        }

        public Projects AddProjects(Projects projects)
        {
            using (ProjectManagerEntities db = new ProjectManagerEntities())
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        Users obj = projects.Users.Count > 0 ? projects.Users.First() : null;
                        projects.Users = null;
                        projects.Tasks = null;
                        db.Projects.Add(projects);
                        db.SaveChanges();
                        if (obj!=null)
                        {
                            obj.Project_ID = projects.Project_ID;
                            obj.Projects = null;
                            obj.Tasks = null;
                            db.Entry(obj).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }

            return projects;

        }

        public Projects DeleteProjects(int id)
        {
            Projects projects = null;

            using (ProjectManagerEntities db = new ProjectManagerEntities())
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    
                    try
                    {
                        projects = db.Projects.Find(id);
                        if (projects == null)
                        {
                            return null;
                        }
                        var linkedUsers = db.Users.FirstOrDefault(x => x.Project_ID == id);
                        if (linkedUsers!=null)
                        {
                            linkedUsers.Project_ID = null;
                            db.Entry(linkedUsers).State = EntityState.Modified;
                        }
                        db.Projects.Remove(projects);
                        db.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                    }
                }
            }

            return projects;
        }

        
        public bool ProjectsExists(int id)
        {
            return db.Projects.Count(e => e.Project_ID == id) > 0;
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }

    public class Project : Projects {
        public int ManagerID { get; set; }
    }

}
