﻿using ProjectManager.DataLayer;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.BusinessLayer
{
    public class TasksBAL : IDisposable
    {

        private ProjectManagerEntities db = new ProjectManagerEntities();

        public TasksBAL() { }

        public TasksBAL(IProjectManagerContext context)
        {
            //db = context;
        }


        public IQueryable<Tasks> GetTasks()
        {
            return db.Tasks;
        }

        public Tasks GetTasks(int id)
        {
            return db.Tasks.Find(id);

        }


        public void UpdateTasks(Tasks tasks)
        {
            using (ProjectManagerEntities db = new ProjectManagerEntities())
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        Tasks objTasks = db.Tasks.FirstOrDefault(x => x.Task_ID == tasks.Task_ID);
                        db.Entry(objTasks).CurrentValues.SetValues(tasks);
                        tasks.Users = null;
                        tasks.Projects = null;
                        tasks.Parent_Tasks = null;
                        db.Entry(objTasks).State = EntityState.Modified;

                        db.SaveChanges();

                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }

                }
            }

        }

        public Tasks AddTasks(Tasks tasks)
        {
            using (ProjectManagerEntities db = new ProjectManagerEntities())
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        var userID = (tasks.Users.FirstOrDefault()).User_ID;
                        Users user = db.Users.FirstOrDefault( x=> x.User_ID == userID);
                        tasks.Users = null;
                        db.Tasks.Add(tasks);
                        db.SaveChanges();
                        user.Tasks = null;
                        user.Projects = null;
                        user.Task_ID = tasks.Task_ID;
                        db.Entry(user).State = EntityState.Modified;
                        db.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        
                    }
                }
            }

            return tasks;

        }

        public Tasks DeleteTasks(int id)
        {
            Tasks tasks = null;

            using (ProjectManagerEntities db = new ProjectManagerEntities())
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {

                    try
                    {
                        tasks = db.Tasks.Find(id);
                        
                        var user = db.Users.FirstOrDefault(x => x.Task_ID == id);
                        if (tasks == null)
                        {
                            return tasks;
                        }
                        if (user!=null)
                        {
                            user.Task_ID = null;
                            db.Entry(user).State = EntityState.Modified;
                        }                        
                        db.Tasks.Remove(tasks);
                        db.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                    }
                }
            }

            return tasks;
        }


        public bool TasksExists(int id)
        {
            return db.Tasks.Count(e => e.Task_ID == id) > 0;
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}
