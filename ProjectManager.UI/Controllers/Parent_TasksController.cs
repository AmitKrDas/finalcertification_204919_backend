﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ProjectManager.DataLayer;

namespace ProjectManager.UI
{
    public class Parent_TasksController : ApiController
    {
        private ProjectManagerEntities db = new ProjectManagerEntities();

        // GET: api/Parent_Tasks
        public IQueryable<Parent_Tasks> GetParent_Tasks()
        {
            return db.Parent_Tasks;
        }

        // GET: api/Parent_Tasks/5
        [ResponseType(typeof(Parent_Tasks))]
        public IHttpActionResult GetParent_Tasks(int id)
        {
            Parent_Tasks parent_Tasks = db.Parent_Tasks.Find(id);
            if (parent_Tasks == null)
            {
                return NotFound();
            }

            return Ok(parent_Tasks);
        }

        // PUT: api/Parent_Tasks/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutParent_Tasks(int id, Parent_Tasks parent_Tasks)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != parent_Tasks.Parent_ID)
            {
                return BadRequest();
            }

            db.Entry(parent_Tasks).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Parent_TasksExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Parent_Tasks
        [ResponseType(typeof(Parent_Tasks))]
        public IHttpActionResult PostParent_Tasks(Parent_Tasks parent_Tasks)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Parent_Tasks.Add(parent_Tasks);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = parent_Tasks.Parent_ID }, parent_Tasks);
        }

        // DELETE: api/Parent_Tasks/5
        [ResponseType(typeof(Parent_Tasks))]
        public IHttpActionResult DeleteParent_Tasks(int id)
        {
            Parent_Tasks parent_Tasks = db.Parent_Tasks.Find(id);
            if (parent_Tasks == null)
            {
                return NotFound();
            }

            db.Parent_Tasks.Remove(parent_Tasks);
            db.SaveChanges();

            return Ok(parent_Tasks);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Parent_TasksExists(int id)
        {
            return db.Parent_Tasks.Count(e => e.Parent_ID == id) > 0;
        }
    }
}