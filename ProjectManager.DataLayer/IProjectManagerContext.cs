﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.DataLayer
{
    public interface IProjectManagerContext: IDisposable
    {
        DbSet<Projects> Projects { get; }
        int SaveChanges();
        void MarkAsModified(Projects item);
    }
}
