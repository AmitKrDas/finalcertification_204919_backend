﻿using ProjectManager.BusinessLayer;
using ProjectManager.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.PerformanceTest
{
    public class TasksTest
    {
        public void GetTasks_ShouldReturnAllTasks()
        {
            TasksBAL objBAL = new TasksBAL();
            List<Tasks> result = objBAL.GetTasks().ToList();
        }

       
        public void GetTasks_ShouldRetrunSingleTask()
        {
            TasksBAL objBAL = new TasksBAL();
            Tasks result = objBAL.GetTasks(24);
        }

       
        public void TasksExists_ShouldRetrunifExists()
        {
            TasksBAL objBAL = new TasksBAL();

            var result = objBAL.TasksExists(24);
        }

       
        public void AddTasks_ShouldAddTask()
        {
            TasksBAL objBAL = new TasksBAL();
            UsersBAL userBAL = new UsersBAL();
            Users user = userBAL.GetUsers(30);
            Tasks Task = new Tasks()
            {
                Task = "New Task U" + new Random().Next(1, 2000),
                Parent_ID = 16,
                Project_ID = 38,
                Priority = 10,
                Start_Date = DateTime.Now,
                End_Date = DateTime.Now.AddDays(new Random().Next(20, 100)),
                Status = "active",
                Users = new Users[1] { user }
            };

            var result = objBAL.AddTasks(Task);
        }

       
        public void UpdateTasks_ShouldUpdateTask()
        {
            TasksBAL objBAL = new TasksBAL();
            Tasks task = objBAL.GetTasks(23);
            task.Task = "Task 3 -2 -updated";
            objBAL.UpdateTasks(task);
        }
    }
}
