﻿using NBench;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.PerformanceTest
{
    public class PerformanceTest_Users
    {
        UserTest objUsertest = new UserTest();

        [PerfBenchmark(NumberOfIterations = 1,
          RunMode = RunMode.Throughput,
          TestMode = TestMode.Test,
          SkipWarmups = true, Description = "Get All Users")]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 2000)]
        public void GetAll_Benchmark_Performance()
        {
            for (var i = 0; i < 100; i++)
            {
                objUsertest.GetUsers_ShouldReturnAllUsers();
            }
        }

        [PerfBenchmark(NumberOfIterations = 1,
           RunMode = RunMode.Throughput,
           TestMode = TestMode.Test,
           SkipWarmups = true, Description = "Get Single Users")]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 2000)]
        public void GetSingle_Benchmark_Performance()
        {
            for (var i = 0; i < 100; i++)
            {
                objUsertest.GetUsers_ShouldRetrunSingleUser();
            }
        }

        [PerfBenchmark(NumberOfIterations = 1,
           RunMode = RunMode.Throughput,
           TestMode = TestMode.Test,
           SkipWarmups = true, Description = "Check if user exists")]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 2000)]
        public void CheckifExists_Benchmark_Performance()
        {
            for (var i = 0; i < 100; i++)
            {
                objUsertest.UsersExists_ShouldRetrunifExists();
            }
        }

        [PerfBenchmark(NumberOfIterations = 1,
           RunMode = RunMode.Throughput,
           TestMode = TestMode.Test,
           SkipWarmups = true, Description ="Add Users")]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 2000)]
        public void Add_Benchmark_Performance()
        {
            for (var i = 0; i < 100; i++)
            {
                objUsertest.AddUsers_ShouldAddUser();
            }
        }

        [PerfBenchmark(NumberOfIterations = 1,
           RunMode = RunMode.Throughput,
           TestMode = TestMode.Test,
           SkipWarmups = true, Description = "Update Users")]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 2000)]
        public void Update_Benchmark_Performance()
        {
            for (var i = 0; i < 100; i++)
            {
                objUsertest.UpdateUsers_ShouldUpdateUser();
            }
        }

       
    }
}
