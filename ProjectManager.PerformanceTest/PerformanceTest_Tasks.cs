﻿using NBench;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.PerformanceTest
{
    public class PerformanceTest_Tasks
    {
        TasksTest objTasksTest = new TasksTest();

        [PerfBenchmark(NumberOfIterations = 1,
          RunMode = RunMode.Throughput,
          TestMode = TestMode.Test,
          SkipWarmups = true, Description = "Get All Tasks")]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 2000)]
        public void GetAll_Benchmark_Performance()
        {
            for (var i = 0; i < 100; i++)
            {
                objTasksTest.GetTasks_ShouldReturnAllTasks();
            }
        }

        [PerfBenchmark(NumberOfIterations = 1,
          RunMode = RunMode.Throughput,
          TestMode = TestMode.Test,
          SkipWarmups = true, Description = "Get single task")]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 2000)]
        public void GetSingle_Benchmark_Performance()
        {
            for (var i = 0; i < 100; i++)
            {
                objTasksTest.GetTasks_ShouldRetrunSingleTask();
            }
        }

        [PerfBenchmark(NumberOfIterations = 1,
         RunMode = RunMode.Throughput,
         TestMode = TestMode.Test,
         SkipWarmups = true, Description = "Add Task")]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 2000)]
        public void Add_Benchmark_Performance()
        {
            for (var i = 0; i < 100; i++)
            {
                objTasksTest.AddTasks_ShouldAddTask();
            }
        }

        [PerfBenchmark(NumberOfIterations = 1,
         RunMode = RunMode.Throughput,
         TestMode = TestMode.Test,
         SkipWarmups = true, Description = "Update Task")]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 2000)]
        public void Update_Benchmark_Performance()
        {
            for (var i = 0; i < 100; i++)
            {
                objTasksTest.UpdateTasks_ShouldUpdateTask();
            }
        }
    }
}
