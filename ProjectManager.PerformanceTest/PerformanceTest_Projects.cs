﻿using NBench;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.PerformanceTest
{
    public class PerformanceTest_Projects
    {
        ProjectsTest objProjectsTest = new ProjectsTest();

        [PerfBenchmark(NumberOfIterations = 1,
           RunMode = RunMode.Throughput,
           TestMode = TestMode.Test,
           SkipWarmups = true, Description = "Get All Projects")]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 2000)]
        public void GetAll_Benchmark_Performance()
        {
            for (var i = 0; i < 100; i++)
            {
                objProjectsTest.GetProjects_ShouldReturnAllProjects();
            }
        }

        [PerfBenchmark(NumberOfIterations = 1,
           RunMode = RunMode.Throughput,
           TestMode = TestMode.Test,
           SkipWarmups = true, Description = "Get single Projects")]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 2000)]
        public void GetSingle_Benchmark_Performance()
        {
            for (var i = 0; i < 100; i++)
            {
                objProjectsTest.GetProjects_ShouldRetrunSingleProject();
            }
        }

        [PerfBenchmark(NumberOfIterations = 1,
           RunMode = RunMode.Throughput,
           TestMode = TestMode.Test,
           SkipWarmups = true, Description = "Search Projects")]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 2000)]
        public void Search_Benchmark_Performance()
        {
            for (var i = 0; i < 100; i++)
            {
                objProjectsTest.SearchProjects_ShouldRetrunSearchedProjects();
            }
        }

        [PerfBenchmark(NumberOfIterations = 1,
           RunMode = RunMode.Throughput,
           TestMode = TestMode.Test,
           SkipWarmups = true, Description = "Add Projects")]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 2000)]
        public void Add_Benchmark_Performance()
        {
            for (var i = 0; i < 100; i++)
            {
                objProjectsTest.AddProjects_ShouldAddProject();
            }
        }

        [PerfBenchmark(NumberOfIterations = 1,
           RunMode = RunMode.Throughput,
           TestMode = TestMode.Test,
           SkipWarmups = true, Description = "Update Projects")]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 2000)]
        public void Update_Benchmark_Performance()
        {
            for (var i = 0; i < 100; i++)
            {
                objProjectsTest.UpdateProjects_ShouldUpdateProject();
            }
        }

        
    }
}
