﻿using ProjectManager.BusinessLayer;
using ProjectManager.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.PerformanceTest
{
    public class ProjectsTest
    {
        public void GetProjects_ShouldReturnAllProjects()
        {
            ProjectBAL objBAL = new ProjectBAL();
            List<Projects> result = objBAL.GetProjects().ToList();            
        }

        
        public void GetProjects_ShouldRetrunSingleProject()
        {
            ProjectBAL objBAL = new ProjectBAL();
            Projects result = objBAL.GetProjects(34);
        }

       
        public void SearchProjects_ShouldRetrunSearchedProjects()
        {
            ProjectBAL objBAL = new ProjectBAL();
            var searchText = "test";
            List<Projects> result = objBAL.Search(searchText).ToList();
        }

        public void ProjectsExists_ShouldRetrunifExists()
        {
            ProjectBAL objBAL = new ProjectBAL();

            var result = objBAL.ProjectsExists(31);
        }

       
        public void AddProjects_ShouldAddProject()
        {
            ProjectBAL objBAL = new ProjectBAL();
            //UsersBAL objUserBAL = new UsersBAL();
            Users manager = new Users() { User_ID = 24, First_Name = "Ayan", Last_Name = "Sen", Employee_ID = "EMP008", Task_ID = 21 };

            Projects newProj = new Projects()
            {
                Project = "Project " + new Random().Next(1, 2000),
                Priority = 10,
                Start_Date = DateTime.Now,
                End_Date = DateTime.Now.AddDays(30),
                //Users = new Users[1] { }
                Users = new Users[1] { manager }
            };
            var result = objBAL.AddProjects(newProj);

        }

        
        public void UpdateProjects_ShouldUpdateProject()
        {
            ProjectBAL objBAL = new ProjectBAL();
            Projects project = objBAL.GetProjects(52);
            var updatedProjName = "Proj updated";
            project.Project = updatedProjName;

            objBAL.UpdateProjects(project.Project_ID, project);

        }

    }
}
