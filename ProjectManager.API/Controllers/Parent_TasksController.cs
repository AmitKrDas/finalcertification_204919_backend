﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using ProjectManager.BusinessLayer;
using ProjectManager.DataLayer;

namespace ProjectManager.API
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class Parent_TasksController : ApiController
    {
       private ParentTasksBAL parentTasksBAL = new ParentTasksBAL();

        // GET: api/Parent_Tasks
        public IQueryable<Parent_Tasks> GetParent_Tasks()
        {
            return parentTasksBAL.GetParentTasks();
        }

        // GET: api/Parent_Tasks/5
        [ResponseType(typeof(Parent_Tasks))]
        public IHttpActionResult GetParent_Tasks(int id)
        {
            Parent_Tasks parent_Tasks = parentTasksBAL.GetParentTasks(id);
            if (parent_Tasks == null)
            {
                return NotFound();
            }

            return Ok(parent_Tasks);
        }

        // PUT: api/Parent_Tasks/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutParent_Tasks(int id, Parent_Tasks parent_Tasks)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != parent_Tasks.Parent_ID)
            {
                return BadRequest();
            }

            
            try
            {
                parentTasksBAL.UpdateParentTasks(parent_Tasks);
            }
            catch (Exception)
            {
                if (!parentTasksBAL.Parent_TasksExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Parent_Tasks
        [ResponseType(typeof(Parent_Tasks))]
        public IHttpActionResult PostParent_Tasks(Parent_Tasks parentTasks)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            parentTasks = parentTasksBAL.AddParentTasks(parentTasks);

            return CreatedAtRoute("DefaultApi", new { id = parentTasks.Parent_ID }, parentTasks);
        }

        // DELETE: api/Parent_Tasks/5
        [ResponseType(typeof(Parent_Tasks))]
        public IHttpActionResult DeleteParent_Tasks(int id)
        {
            Parent_Tasks parentTasks = parentTasksBAL.DeleteParentTasks(id);

            if (parentTasks == null)
            {
                return NotFound();
            }
            return Ok(parentTasks);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        
    }
}