﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using ProjectManager.DataLayer;
using ProjectManager.BusinessLayer;

namespace ProjectManager.API
{
    [EnableCors(origins: "*", headers:"*", methods:"*")]
    public class ProjectsController : ApiController
    {
        //private ProjectManagerEntities db = new ProjectManagerEntities();
        private ProjectBAL projectBAL = new ProjectBAL();

        public ProjectsController() { projectBAL = new ProjectBAL(); }
        public ProjectsController(IProjectManagerContext context) { projectBAL = new ProjectBAL(context); }
        // GET: api/Projects
        public IEnumerable<Projects> GetProjects()
        {
            return projectBAL.GetProjects();
        }

        // GET: api/Projects
        [System.Web.Http.AcceptVerbs("GET")]
        [System.Web.Http.HttpGet]
        [Route("api/Projects/Search/{searchText}")]
        public IEnumerable<Projects> Search(string searchText)
        {
            return projectBAL.Search(searchText);
        }

        // GET: api/Projects/5
        [ResponseType(typeof(Projects))]
        public IHttpActionResult GetProjects(int id)
        {
            Projects projects = projectBAL.GetProjects(id);

            if (projects == null)
            {
                return NotFound();
            }

            return Ok(projects);
        }

        // PUT: api/Projects/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutProjects(int id, Projects projects)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != projects.Project_ID)
            {
                return BadRequest();
            }
            

            try
            {
                projectBAL.UpdateProjects(id, projects);
            }
            catch (Exception)
            {
                if (!projectBAL.ProjectsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Projects
        [ResponseType(typeof(Projects))]
        public IHttpActionResult PostProjects(Projects projects)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            projects = projectBAL.AddProjects(projects);

            return CreatedAtRoute("DefaultApi", new { id = projects.Project_ID }, projects);
        }

        // DELETE: api/Projects/5
        [ResponseType(typeof(Projects))]
        public IHttpActionResult DeleteProjects(int id)
        {
            Projects projects = projectBAL.DeleteProjects(id);
            if (projects == null)
            {
                return NotFound();
            }           
            return Ok(projects);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

       
    }
}