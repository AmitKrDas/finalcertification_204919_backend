﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using ProjectManager.BusinessLayer;
using ProjectManager.DataLayer;

namespace ProjectManager.API
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class TasksController : ApiController
    {
        private TasksBAL taskBAL = new TasksBAL();

        // GET: api/Tasks
        public IQueryable<Tasks> GetTasks()
        {
            return taskBAL.GetTasks();
        }

        // GET: api/Tasks/5
        [ResponseType(typeof(Tasks))]
        public IHttpActionResult GetTasks(int id)
        {
            Tasks tasks = taskBAL.GetTasks(id);
            if (tasks == null)
            {
                return NotFound();
            }

            return Ok(tasks);
        }

        // PUT: api/Tasks/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTasks(int id, Tasks tasks)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tasks.Task_ID)
            {
                return BadRequest();
            }
            try
            {
                taskBAL.UpdateTasks(tasks);
            }

            catch (Exception)
            {
                if (!taskBAL.TasksExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Tasks
        [ResponseType(typeof(Tasks))]
        public IHttpActionResult PostTasks(Tasks tasks)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            tasks = taskBAL.AddTasks(tasks);

            return CreatedAtRoute("DefaultApi", new { id = tasks.Task_ID }, tasks);
        }

        // DELETE: api/Tasks/5
        [ResponseType(typeof(Tasks))]
        public IHttpActionResult DeleteTasks(int id)
        {
            Tasks tasks = taskBAL.DeleteTasks(id);

            return Ok(tasks);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        
    }
}