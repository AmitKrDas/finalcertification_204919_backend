﻿using NUnit.Framework;
using ProjectManager.BusinessLayer;
using ProjectManager.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.NUNIT.Tests
{
    [TestFixture]
    public class TestUsersBAL
    {
        int userID;
        [TestCase]
        public void GetUsers_ShouldReturnAllUsers()
        {
            UsersBAL objBAL = new UsersBAL();
            List<Users> result = objBAL.GetUsers().ToList();
            Assert.Greater(result.Count, 0);
        }

        [TestCase]
        public void GetUsers_ShouldRetrunSingleUser()
        {
            UsersBAL objBAL = new UsersBAL();
            Users result = objBAL.GetUsers(21);
            Assert.AreEqual(result.First_Name, "Tony");
        }

        [TestCase]
        public void UsersExists_ShouldRetrunifExists()
        {
            UsersBAL objBAL = new UsersBAL();

            var result = objBAL.UsersExists(24);

            Assert.AreEqual(result, true);
        }

        [TestCase]
        public void AddUsers_ShouldAddUser()
        {
            UsersBAL objBAL = new UsersBAL();
            Users user = new Users() { First_Name = "Amit" + new Random().Next(1, 2000), Last_Name = "Sen", Employee_ID = "EMP1"+ new Random().Next(1, 2000)};

            var result = objBAL.AddUsers(user);
            userID = result.User_ID;
            Assert.Greater(result.User_ID, 0);
        }

        [TestCase]
        public void UpdateUsers_ShouldUpdateUser()
        {
            UsersBAL objBAL = new UsersBAL();
            Users user = objBAL.GetUsers(24);
            user.First_Name = "Ayan-updated";
            objBAL.UpdateUsers(user);

            var result = objBAL.GetUsers(user.User_ID);

            Assert.AreEqual(result.First_Name, user.First_Name);
        }

        
        [TestCase]
        public void DeleteUsers_ShouldDeleteUser()
        {
            UsersBAL objBAL = new UsersBAL();

            var result = objBAL.DeleteUsers(userID);

            Assert.AreEqual(false, objBAL.UsersExists(result.User_ID));
        }





    }
}
