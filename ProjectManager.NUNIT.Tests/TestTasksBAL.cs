﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectManager.BusinessLayer;
using ProjectManager.DataLayer;

namespace ProjectManager.NUNIT.Tests
{
    [TestFixture]
    public class TestTasksBAL
    {
        int taskID;
        [TestCase]
        public void GetTasks_ShouldReturnAllTasks()
        {
            TasksBAL objBAL = new TasksBAL();
            List<Tasks> result = objBAL.GetTasks().ToList();
            Assert.Greater(result.Count, 0);
        }

        [TestCase]
        public void GetTasks_ShouldRetrunSingleTask()
        {
            TasksBAL objBAL = new TasksBAL();
            Tasks result = objBAL.GetTasks(24);
            Assert.AreEqual(result.Task, "New Task");
        }

        [TestCase]
        public void TasksExists_ShouldRetrunifExists()
        {
            TasksBAL objBAL = new TasksBAL();

            var result = objBAL.TasksExists(24);

            Assert.AreEqual(result, true);
        }

        [TestCase]
        public void AddTasks_ShouldAddTask()
        {
            TasksBAL objBAL = new TasksBAL();
            UsersBAL userBAL = new UsersBAL();
            Users user = userBAL.GetUsers(30);
            Tasks Task = new Tasks() { Task = "New Task U" + new Random().Next(1, 2000), Parent_ID = 16, 
                Project_ID = 38, Priority = 10, Start_Date = DateTime.Now, End_Date = DateTime.Now.AddDays(new Random().Next(20,100)), Status = "active", Users = new Users[1] { user }
            };

            var result = objBAL.AddTasks(Task);
            taskID = result.Task_ID;
            Assert.Greater(result.Task_ID, 0);
        }

        [TestCase]
        public void UpdateTasks_ShouldUpdateTask()
        {
            TasksBAL objBAL = new TasksBAL();
            Tasks task = objBAL.GetTasks(23);
            task.Task = "Task 3 -2 -updated";
            
            objBAL.UpdateTasks(task);

            var result = objBAL.GetTasks(task.Task_ID);

            Assert.AreEqual(result.Task, task.Task);
        }

        public void DeleteTasks_ShouldDeleteTask()
        {
            TasksBAL objBAL = new TasksBAL();

            var result = objBAL.DeleteTasks(taskID);

            Assert.AreEqual(false, objBAL.TasksExists(result.Task_ID));
        }
    }
}
