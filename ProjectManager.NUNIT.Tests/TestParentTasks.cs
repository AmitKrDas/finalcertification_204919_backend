﻿using NUnit.Framework;
using ProjectManager.BusinessLayer;
using ProjectManager.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ProjectManager.NUNIT.Tests
{
    [TestFixture]
    public class TestParentTasks
    {
        int parentTaskID;

        [TestCase]
        public void GetParentTasks_ShouldReturnAllParentTasks()
        {
            ParentTasksBAL objBAL = new ParentTasksBAL();
            List<Parent_Tasks> result = objBAL.GetParentTasks().ToList();
            Assert.Greater(result.Count, 0);
        }

        [TestCase]
        public void GetParentTasks_ShouldRetrunSingleParentTask()
        {
            ParentTasksBAL objBAL = new ParentTasksBAL();
            Parent_Tasks result = objBAL.GetParentTasks(15);
            Assert.AreEqual(result.Parent_Task, "test Parent");
        }

        [TestCase]
        public void ParentTasksExists_ShouldReturnifExists()
        {
            ParentTasksBAL objBAL = new ParentTasksBAL();

            var result = objBAL.Parent_TasksExists(15);

            Assert.AreEqual(result, true);
        }

        [TestCase]
        public void AddParentTasks_ShouldAddParentTask()
        {
            ParentTasksBAL objBAL = new ParentTasksBAL();
            UsersBAL userBAL = new UsersBAL();
            
            Parent_Tasks ParentTask = new Parent_Tasks() { 
            
               Parent_Task = "New ParentTask U" + new Random().Next(1, 2000)
                
            };

            var result = objBAL.AddParentTasks(ParentTask);
            parentTaskID = result.Parent_ID;
            Assert.Greater(result.Parent_ID, 0);
        }

        [TestCase]
        public void UpdateParentTasks_ShouldUpdateParentTask()
        {
            ParentTasksBAL objBAL = new ParentTasksBAL();
            Parent_Tasks parentTask = objBAL.GetParentTasks(15);
            parentTask.Parent_Task = "test Parent";

            objBAL.UpdateParentTasks(parentTask);

            var result = objBAL.GetParentTasks(parentTask.Parent_ID);

            Assert.AreEqual(result.Parent_Task, parentTask.Parent_Task);
        }

        [TestCase]
        public void deleteparenttasks_shoulddeleteparenttask()
        {
            ParentTasksBAL objbal = new ParentTasksBAL();

            var result = objbal.DeleteParentTasks(parentTaskID);

            if (result != null)
            {
                Assert.AreEqual(false, objbal.Parent_TasksExists(result.Parent_ID));
            }
        }
    }
}
