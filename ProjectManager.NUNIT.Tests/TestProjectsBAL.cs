﻿using Newtonsoft.Json;
using NUnit.Framework;
using ProjectManager.BusinessLayer;
using ProjectManager.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.NUNIT.Tests
{
    [TestFixture]
    public class TestProjectsBAL
    {
        int projectID;
        [TestCase]
        public void GetProjects_ShouldReturnAllProjects()
        {
            ProjectBAL objBAL = new ProjectBAL();
            List<Projects> result = objBAL.GetProjects().ToList();
            Assert.Greater(result.Count, 0);
        }

        [TestCase]
        public void GetProjects_ShouldRetrunSingleProject()
        {
            ProjectBAL objBAL = new ProjectBAL();
            Projects result = objBAL.GetProjects(34);
            Assert.AreEqual(result.Project, "Proj 3");
        }

        [TestCase]
        public void SearchProjects_ShouldRetrunSearchedProjects()
        {
            ProjectBAL objBAL = new ProjectBAL();
            var searchText = "test";
            List<Projects> result = objBAL.Search(searchText).ToList();
            List<Projects> obj = result.FindAll(x => x.Project.Contains(searchText)).ToList();

            Assert.AreEqual(result.Count, obj.Count);
        }

        [TestCase]
        public void ProjectsExists_ShouldRetrunifExists()
        {
            ProjectBAL objBAL = new ProjectBAL();
            
            var result = objBAL.ProjectsExists(31);
            
            Assert.AreEqual(result, true);
        }

        [TestCase]
        public void AddProjects_ShouldAddProject()
        {
            ProjectBAL objBAL = new ProjectBAL();
            //UsersBAL objUserBAL = new UsersBAL();
            Users manager = new Users() { User_ID = 24, First_Name = "Ayan", Last_Name = "Sen", Employee_ID = "EMP008", Task_ID = 21 };

            Projects newProj = new Projects()
            {
                Project = "Project "+ new Random().Next(1,2000),
                Priority = 10,
                Start_Date = DateTime.Now,
                End_Date = DateTime.Now.AddDays(30),
                //Users = new Users[1] { }
                Users = new Users[1] { manager}
            };
            var result = objBAL.AddProjects(newProj);

            projectID = result.Project_ID;

            Assert.Greater(result.Project_ID, 0);
        }

        [TestCase]
        public void UpdateProjects_ShouldUpdateProject()
        {
            ProjectBAL objBAL = new ProjectBAL();
            Projects project = objBAL.GetProjects(52);
            var updatedProjName = "Proj updated";
            project.Project = updatedProjName;
           
            objBAL.UpdateProjects(project.Project_ID, project);

            var result = objBAL.GetProjects(project.Project_ID);

            Assert.AreEqual(result.Project, project.Project);
        }

        [TestCase]
        public void DeleteProjects_ShouldDeleteProject()
        {
            ProjectBAL objBAL = new ProjectBAL();
            
            var result = objBAL.DeleteProjects(projectID);

            Assert.AreEqual(false,objBAL.ProjectsExists(result.Project_ID));
        }
        
    }
}
